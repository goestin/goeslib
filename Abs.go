package goeslib

import (
	"os"
	"path/filepath"
	"strings"
)

// Abs reads a relative path name and return a full path name.
func Abs(p *string) (string, error) {

	var x string
	var err error

	switch {
	case strings.HasPrefix(*p, "~"):
		e := os.Environ()
		for _, kv := range e {
			if strings.HasPrefix(kv, "HOME=") {
				h := strings.Split(kv, "=")[1]
				x = h + string(*p)[1:]
			}
		}
	case strings.HasPrefix(*p, "./"):
		x, err = filepath.Abs(*p)
	case strings.HasPrefix(*p, "/"):
		x = *p
	default:
		x, err = filepath.Abs(*p)
	}

	return x, err
}
