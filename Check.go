package goeslib

// Check provides an easy if err != nill panic mechanism (for prototyping.)
func Check(e error) {
	if e != nil {
		panic(e)
	}
}
