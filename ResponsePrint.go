package goeslib

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

// ReponsePrint quickly prints de http reponse body for debugging purposes.
func ResponsePrint(r *http.Response) {
	responseData, err := ioutil.ReadAll(r.Body)
	if err != nil {
		panic(err)
	}
	fmt.Printf("--- REPONSE ---:\n %s \n --- END RESPONSE ---", string(responseData))
}
