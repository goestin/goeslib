package goeslib

// StringInSlice : check if a string is present in a slice of strings.
func StringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}
